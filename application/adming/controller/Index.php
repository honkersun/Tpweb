<?php
namespace app\adming\controller;

use think\Controller;
use app\adming\model\AdmingModel;

class Index extends Controller
{
    public function index()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id='".$_SESSION['admin_id']."'");
        $this->assign('find_admin_user',$find_admin_user);
 
        $category_tree = AdmingModel::get_category_tree();
        $this->assign('category_tree',$category_tree);
        
        $this->assign('cat_name','类目管理');
        return $this->fetch();
    }
    public function arclist()
    {	
        $find_admin_user = AdmingModel::find_admin_user("and admin_id='".$_SESSION['admin_id']."'");
        $this->assign('find_admin_user',$find_admin_user);
        
        $where = '';
        $_GET['title'] = isset($_GET['title'])?$_GET['title']:'';
        $_GET['catid'] = isset($_GET['catid'])?$_GET['catid']:'';
        $_GET['brand_id'] = isset($_GET['brand_id'])?$_GET['brand_id']:'';
        $_GET['status'] = isset($_GET['status'])?$_GET['status']:'';
        $_GET['flag'] = isset($_GET['flag'])?$_GET['flag']:'';
        $_GET['user_id'] = isset($_GET['user_id'])?$_GET['user_id']:'';
        if(!empty($_GET['title'])){
            $where .= " and title like '%".$_GET['title']."%'";
        }
        if(!empty($_GET['catid'])){
            $child_catid = AdmingModel::getChildCatid($_GET['catid']);
            $where .= " and catid in (".$child_catid.")";
        }
        if(!empty($_GET['brand_id'])){
            $where .= " and brand_id='".$_GET['brand_id']."'";
        }
        if(!empty($_GET['status'])){
            $where .= " and status='".$_GET['status']."'";
        }
        if(!empty($_GET['flag'])){
            $where .= " and flag_".$_GET['flag']."=1";
        }
        if(!empty($_GET['user_id'])){
            $where .= " and user_id='".$_GET['user_id']."'";
        }
        
        $limit = !empty($_GET['limit'])?$_GET['limit']:15;
        $page = !empty($_GET['page'])?$_GET['page']:1;
        $params = ['page'=>$page,'query'=>['title'=>$_GET['title'],'catid'=>$_GET['catid'],'brand_id'=>$_GET['brand_id'],'status'=>$_GET['status'],'flag'=>$_GET['flag'],'user_id'=>$_GET['user_id']]];
        $query = db('arclist')->where("1=1 $where")->order('arcid desc')->paginate($limit,false,$params);
        $page_show = $query->render();
        $this->assign('page_show',$page_show);
        $this->assign('arclist_list',$query);
        
        $brand_list = AdmingModel::select_brand();
        $this->assign('brand_list',$brand_list);
        
        $admin_user_list = AdmingModel::select_admin_user();
        $this->assign('admin_user_list',$admin_user_list);
        
        $category_tree = AdmingModel::get_category_tree();
        $this->assign('category_tree',$category_tree);
        
        $this->assign('arclist_status_arr',AdmingModel::arclist_status_arr());
        $this->assign('arclist_flagshow_arr',AdmingModel::arclist_flagshow_arr());
        
        return $this->fetch();
    }
    public function is_login()
    {
        $username = trim($_POST['username']);
        $password = md5(trim($_POST['password']));
		
        $find_admin_user = AdmingModel::find_admin_user("and username='".$username."' and password='".$password."'");
        if($find_admin_user){
            $_SESSION['admin_id'] = $find_admin_user['admin_id'];
            $_SESSION['username'] = $find_admin_user['username'];
            echo 'success';
        }else{
            echo 'error';
        }
    }
    public function add_category()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id=".$_SESSION['admin_id']);
        $this->assign('find_admin_user',$find_admin_user);
        
        $find_category = AdmingModel::find_category("and catid=".$_GET['catid']);
        $find_category['cat_name'] = isset($find_category['cat_name'])?$find_category['cat_name']:'无';
        $this->assign('find_category',$find_category);
        
        $this->assign('category_nav_arr',AdmingModel::category_nav_arr());
        
        return $this->fetch();
    }
    public function act_add_category()
    {
        $data['image'] = AdmingModel::upload_file($_FILES['image'],1);
        $data['cat_name'] = trim($_POST['cat_name']);
        $data['parent'] = trim($_POST['parent']);
        $data['sort'] = trim($_POST['sort']);
        $data['context'] = trim($_POST['context']);
        $data['title'] = trim($_POST['title']);
        $data['keywords'] = trim($_POST['keywords']);
        $data['description'] = trim($_POST['description']);
        $data['url_name'] = trim($_POST['url_name']);
        $data['nav'] = trim($_POST['nav']);
        $result = AdmingModel::add_category($data);
        if($result){
            $this->success('success','/home.php/adming/index/index.html');
        }else{
            $this->success('操作失败');
        }
    }
    public function edit_category()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id=".$_SESSION['admin_id']);
        $this->assign('find_admin_user',$find_admin_user);
        
        $find_category = AdmingModel::find_category("and catid=".$_GET['catid']);
        $this->assign('find_category',$find_category);
        
        $find_pre_category = AdmingModel::find_category("and catid=".$find_category['parent']);
        $find_pre_category['cat_name'] = isset($find_pre_category['cat_name'])?$find_pre_category['cat_name']:'无';
        $this->assign('find_pre_category',$find_pre_category);
        
        $this->assign('category_nav_arr',AdmingModel::category_nav_arr());
        
        return $this->fetch();
    }
    public function act_save_category()
    {
        $where = "and catid='".$_POST['catid']."'";
        $image = AdmingModel::upload_file($_FILES['image'],1);
        if($image){
            $data['image'] = $image;
        }
        $data['cat_name'] = trim($_POST['cat_name']);
        $data['sort'] = trim($_POST['sort']);
        $data['context'] = trim($_POST['context']);
        $data['title'] = trim($_POST['title']);
        $data['keywords'] = trim($_POST['keywords']);
        $data['description'] = trim($_POST['description']);
        $data['nav'] = trim($_POST['nav']);
        $data['url_name'] = trim($_POST['url_name']);
        if(empty($data['url_name'])){
            $data['url_name'] = 'list';
        }
        $result = AdmingModel::save_category($where,$data);
        if($result){
            $this->success('操作成功','/home.php/adming/index/index.html');
        }else{
            $this->error('操作失败');
        }
    }
    public function act_del_category()
    {
        $where = "and catid='".$_POST['catid']."'";
        $result = AdmingModel::delete_category($where);
        if($result){
            echo 'success';
        }else{
            echo '操作失败';
        }
    }
    public function act_del_arclist()
    {
        $where = "and arcid='".$_POST['arcid']."'";
        $result = AdmingModel::delete_arclist($where);
        if($result){
            echo 'success';
        }else{
            echo '操作失败';
        }
    }
    public function brand()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id=".$_SESSION['admin_id']);
        $this->assign('find_admin_user',$find_admin_user);
        
        $where = '';
        $_GET['brand_name'] = isset($_GET['brand_name'])?$_GET['brand_name']:'';
        if(!empty($_GET['brand_name'])){
            $where .= " and brand_name like '%".$_GET['brand_name']."%'";
        }
        $brand_list = AdmingModel::select_brand($where);
        $this->assign('brand_list',$brand_list);
        
        return $this->fetch();
    }
    public function add_brand()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id=".$_SESSION['admin_id']);
        $this->assign('find_admin_user',$find_admin_user);
        
        return $this->fetch();
    }
    public function act_add_brand()
    {
        $data['brand_name'] = trim($_POST['brand_name']);
        $data['sort'] = trim($_POST['sort']);
        $result = AdmingModel::add_brand($data);
        if($result){
            echo 'success';
        }else{
            echo '操作失败';
        }
    }
    public function edit_brand()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id=".$_SESSION['admin_id']);
        $this->assign('find_admin_user',$find_admin_user);
        
        $find_brand = AdmingModel::find_brand("and brand_id=".$_GET['brand_id']);
        $this->assign('find_brand',$find_brand);
        return $this->fetch();
    }
    public function act_save_brand()
    {
        $where = "and brand_id='".$_POST['brand_id']."'";
        $data['brand_name'] = trim($_POST['brand_name']);
        $data['sort'] = trim($_POST['sort']);
        $result = AdmingModel::save_brand($where,$data);
        if($result){
            echo 'success';
        }else{
            echo '操作失败';
        }
    }
    public function act_del_brand()
    {
        $where = "and brand_id='".$_POST['brand_id']."'";
        $result = AdmingModel::delete_brand($where);
        if($result){
            echo 'success';
        }else{
            echo '操作失败';
        }
    }
    public function add_arclist()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id=".$_SESSION['admin_id']);
        $this->assign('find_admin_user',$find_admin_user);
        
        $brand_list = AdmingModel::select_brand();
        $this->assign('brand_list',$brand_list);
        
        $category_tree = AdmingModel::get_category_tree();
        $this->assign('category_tree',$category_tree);
        
        $find_home_config = AdmingModel::find_home_config("and id=1");
        $this->assign('find_home_config',$find_home_config);
        
        $this->assign('arclist_status_arr',AdmingModel::arclist_status_arr());
        $this->assign('arclist_flagshow_arr',AdmingModel::arclist_flagshow_arr());
        
        return $this->fetch();
    }
    public function act_add_arclist()
    {
        if(empty($_POST['catid'])){
            $this->error('类目不能为空'); return false;
        }
        
        $data = $_POST;
        $data['image1'] = AdmingModel::upload_file($_FILES['image1'],1);
        $data['image2'] = AdmingModel::upload_file($_FILES['image2'],2);
        $data['image3'] = AdmingModel::upload_file($_FILES['image3'],3);
        $data['image4'] = AdmingModel::upload_file($_FILES['image4'],4);
        $data['image5'] = AdmingModel::upload_file($_FILES['image5'],5);
        $data['attachment'] = AdmingModel::upload_file($_FILES['attachment'],6);
        $data['flag_a'] = isset($_POST['flag_a'])?$_POST['flag_a']:'';
        $data['flag_b'] = isset($_POST['flag_b'])?$_POST['flag_b']:'';
        $data['flag_c'] = isset($_POST['flag_c'])?$_POST['flag_c']:'';
        $data['flag_d'] = isset($_POST['flag_d'])?$_POST['flag_d']:'';
        $data['flag_e'] = isset($_POST['flag_e'])?$_POST['flag_e']:'';
        $data['user_id'] = $_SESSION['admin_id'];
        $data['add_time'] = time();
        $data['status'] = 1;
        $result = AdmingModel::add_arclist($data);
        if($result){
            $this->success('操作成功','/home.php/adming/index/arclist.html');
        }else{
            $this->success('操作失败');
        }
    }
    public function edit_arclist()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id=".$_SESSION['admin_id']);
        $this->assign('find_admin_user',$find_admin_user);
        
        $find_arclist = AdmingModel::find_arclist("and arcid=".$_GET['arcid']);
        $this->assign('find_arclist',$find_arclist);
        
        $brand_list = AdmingModel::select_brand();
        $this->assign('brand_list',$brand_list);
        
        $category_tree = AdmingModel::get_category_tree();
        $this->assign('category_tree',$category_tree);
        
        $find_home_config = AdmingModel::find_home_config("and id=1");
        $this->assign('find_home_config',$find_home_config);
        
        $this->assign('arclist_status_arr',AdmingModel::arclist_status_arr());
        $this->assign('arclist_flagshow_arr',AdmingModel::arclist_flagshow_arr());
        
        return $this->fetch();
    }
    public function act_save_arclist()
    {
        $image1 = AdmingModel::upload_file($_FILES['image1'],1);
        $image2 = AdmingModel::upload_file($_FILES['image2'],2);
        $image3 = AdmingModel::upload_file($_FILES['image3'],3);
        $image4 = AdmingModel::upload_file($_FILES['image4'],4);
        $image5 = AdmingModel::upload_file($_FILES['image5'],5);
        $attachment = AdmingModel::upload_file($_FILES['attachment'],6);
        
        $data = $_POST;
        unset($data['arcid']);
        if($image1){
            $data['image1'] = $image1;
        }
        if($image2){
            $data['image2'] = $image2;
        }
        if($image3){
            $data['image3'] = $image3;
        }
        if($image4){
            $data['image4'] = $image4;
        }
        if($image5){
            $data['image5'] = $image5;
        }
        if($attachment){
            $data['attachment'] = $attachment;
        }
        $data['flag_a'] = isset($_POST['flag_a'])?$_POST['flag_a']:'';
        $data['flag_b'] = isset($_POST['flag_b'])?$_POST['flag_b']:'';
        $data['flag_c'] = isset($_POST['flag_c'])?$_POST['flag_c']:'';
        $data['flag_d'] = isset($_POST['flag_d'])?$_POST['flag_d']:'';
        $data['flag_e'] = isset($_POST['flag_e'])?$_POST['flag_e']:'';
        $data['update_time'] = time();
        $data['status'] = trim($_POST['status']);
        $where = "and arcid='".$_POST['arcid']."'";
        $result = AdmingModel::save_arclist($where,$data);
        if($result){
            $this->success('success','/home.php/adming/index/arclist.html');
        }else{
            $this->success('操作失败');
        }
    }
    public function act_add_goods()
    {
        if(empty($_POST['catid'])){
            $this->error('类目不能为空'); return false;
        }
        
        $data = $_POST;
        $data['image1'] = AdmingModel::upload_file($_FILES['image1'],1);
        $data['image2'] = AdmingModel::upload_file($_FILES['image2'],2);
        $data['image3'] = AdmingModel::upload_file($_FILES['image3'],3);
        $data['image4'] = AdmingModel::upload_file($_FILES['image4'],4);
        $data['image5'] = AdmingModel::upload_file($_FILES['image5'],5);
        $data['attachment'] = AdmingModel::upload_file($_FILES['attachment'],6);
        $data['flag_a'] = isset($_POST['flag_a'])?$_POST['flag_a']:'';
        $data['flag_b'] = isset($_POST['flag_b'])?$_POST['flag_b']:'';
        $data['flag_c'] = isset($_POST['flag_c'])?$_POST['flag_c']:'';
        $data['flag_d'] = isset($_POST['flag_d'])?$_POST['flag_d']:'';
        $data['flag_e'] = isset($_POST['flag_e'])?$_POST['flag_e']:'';
        $data['user_id'] = $_SESSION['admin_id'];
        $data['add_time'] = time();
        $data['status'] = 1;
        $result = AdmingModel::add_arclist($data);
        if($result){
            $this->success('操作成功','/home.php/adming/index/goods_list.html');
        }else{
            $this->success('操作失败');
        }
    }
    public function edit_goods()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id=".$_SESSION['admin_id']);
        $this->assign('find_admin_user',$find_admin_user);
        
        $find_arclist = AdmingModel::find_arclist("and arcid=".$_GET['arcid']);
        $this->assign('find_arclist',$find_arclist);
        
        $brand_list = AdmingModel::select_brand();
        $this->assign('brand_list',$brand_list);
        
        $category_tree = AdmingModel::get_category_tree();
        $this->assign('category_tree',$category_tree);
        
        $find_home_config = AdmingModel::find_home_config("and id=1");
        $this->assign('find_home_config',$find_home_config);
        
        $this->assign('arclist_status_arr',AdmingModel::arclist_status_arr());
        $this->assign('arclist_flagshow_arr',AdmingModel::arclist_flagshow_arr());
        
        return $this->fetch();
    }
    public function act_save_goods()
    {
        $image1 = AdmingModel::upload_file($_FILES['image1'],1);
        $image2 = AdmingModel::upload_file($_FILES['image2'],2);
        $image3 = AdmingModel::upload_file($_FILES['image3'],3);
        $image4 = AdmingModel::upload_file($_FILES['image4'],4);
        $image5 = AdmingModel::upload_file($_FILES['image5'],5);
        $attachment = AdmingModel::upload_file($_FILES['attachment'],6);
        
        $data = $_POST;
        unset($data['arcid']);
        if($image1){
            $data['image1'] = $image1;
        }
        if($image2){
            $data['image2'] = $image2;
        }
        if($image3){
            $data['image3'] = $image3;
        }
        if($image4){
            $data['image4'] = $image4;
        }
        if($image5){
            $data['image5'] = $image5;
        }
        if($attachment){
            $data['attachment'] = $attachment;
        }
        $data['flag_a'] = isset($_POST['flag_a'])?$_POST['flag_a']:'';
        $data['flag_b'] = isset($_POST['flag_b'])?$_POST['flag_b']:'';
        $data['flag_c'] = isset($_POST['flag_c'])?$_POST['flag_c']:'';
        $data['flag_d'] = isset($_POST['flag_d'])?$_POST['flag_d']:'';
        $data['flag_e'] = isset($_POST['flag_e'])?$_POST['flag_e']:'';
        $data['update_time'] = time();
        $data['status'] = trim($_POST['status']);
        $where = "and arcid='".$_POST['arcid']."'";
        $result = AdmingModel::save_arclist($where,$data);
        if($result){
            $this->success('success','/home.php/adming/index/goods_list.html');
        }else{
            $this->success('操作失败');
        }
    }
    public function act_add_news()
    {
        if(empty($_POST['catid'])){
            $this->error('类目不能为空'); return false;
        }
        
        $data = $_POST;
        $data['image1'] = AdmingModel::upload_file($_FILES['image1'],1);
        $data['image2'] = AdmingModel::upload_file($_FILES['image2'],2);
        $data['image3'] = AdmingModel::upload_file($_FILES['image3'],3);
        $data['image4'] = AdmingModel::upload_file($_FILES['image4'],4);
        $data['image5'] = AdmingModel::upload_file($_FILES['image5'],5);
        $data['attachment'] = AdmingModel::upload_file($_FILES['attachment'],6);
        $data['flag_a'] = isset($_POST['flag_a'])?$_POST['flag_a']:'';
        $data['flag_b'] = isset($_POST['flag_b'])?$_POST['flag_b']:'';
        $data['flag_c'] = isset($_POST['flag_c'])?$_POST['flag_c']:'';
        $data['flag_d'] = isset($_POST['flag_d'])?$_POST['flag_d']:'';
        $data['flag_e'] = isset($_POST['flag_e'])?$_POST['flag_e']:'';
        $data['user_id'] = $_SESSION['admin_id'];
        $data['add_time'] = time();
        $data['status'] = 1;
        $result = AdmingModel::add_arclist($data);
        if($result){
            $this->success('操作成功','/home.php/adming/index/news_list.html');
        }else{
            $this->success('操作失败');
        }
    }
    public function edit_news()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id=".$_SESSION['admin_id']);
        $this->assign('find_admin_user',$find_admin_user);
        
        $find_arclist = AdmingModel::find_arclist("and arcid=".$_GET['arcid']);
        $this->assign('find_arclist',$find_arclist);
        
        $brand_list = AdmingModel::select_brand();
        $this->assign('brand_list',$brand_list);
        
        $category_tree = AdmingModel::get_category_tree();
        $this->assign('category_tree',$category_tree);
        
        $find_home_config = AdmingModel::find_home_config("and id=1");
        $this->assign('find_home_config',$find_home_config);
        
        $this->assign('arclist_status_arr',AdmingModel::arclist_status_arr());
        $this->assign('arclist_flagshow_arr',AdmingModel::arclist_flagshow_arr());
        
        return $this->fetch();
    }
    public function act_save_news()
    {
        $image1 = AdmingModel::upload_file($_FILES['image1'],1);
        $image2 = AdmingModel::upload_file($_FILES['image2'],2);
        $image3 = AdmingModel::upload_file($_FILES['image3'],3);
        $image4 = AdmingModel::upload_file($_FILES['image4'],4);
        $image5 = AdmingModel::upload_file($_FILES['image5'],5);
        $attachment = AdmingModel::upload_file($_FILES['attachment'],6);
        
        $data = $_POST;
        unset($data['arcid']);
        if($image1){
            $data['image1'] = $image1;
        }
        if($image2){
            $data['image2'] = $image2;
        }
        if($image3){
            $data['image3'] = $image3;
        }
        if($image4){
            $data['image4'] = $image4;
        }
        if($image5){
            $data['image5'] = $image5;
        }
        if($attachment){
            $data['attachment'] = $attachment;
        }
        $data['flag_a'] = isset($_POST['flag_a'])?$_POST['flag_a']:'';
        $data['flag_b'] = isset($_POST['flag_b'])?$_POST['flag_b']:'';
        $data['flag_c'] = isset($_POST['flag_c'])?$_POST['flag_c']:'';
        $data['flag_d'] = isset($_POST['flag_d'])?$_POST['flag_d']:'';
        $data['flag_e'] = isset($_POST['flag_e'])?$_POST['flag_e']:'';
        $data['update_time'] = time();
        $data['status'] = trim($_POST['status']);
        $where = "and arcid='".$_POST['arcid']."'";
        $result = AdmingModel::save_arclist($where,$data);
        if($result){
            $this->success('success','/home.php/adming/index/news_list.html');
        }else{
            $this->success('操作失败');
        }
    }
    public function edit_home_config()
    {
        $find_home_config = AdmingModel::find_home_config("and id=1");
        $this->assign('find_home_config',$find_home_config);
        
        return $this->fetch();
    }
    public function act_save_home_config()
    {
        $where = "and id=1";
        $logo = AdmingModel::upload_file($_FILES['logo'],1);
        if($logo){
            $data['logo'] = $logo;
        }
        $data['title'] = trim($_POST['title']);
        $data['keywords'] = trim($_POST['keywords']);
        $data['description'] = trim($_POST['description']);
        if(!empty($_POST['arc_field'])){
            $data['arc_field'] = implode(",",$_POST['arc_field']);
        }else{
            $data['arc_field'] = '';
        }
        $result = AdmingModel::save_home_config($where,$data);
        if($result){
            $this->success('操作成功');
        }else{
            $this->success('操作失败');
        }
    }
    public function users()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id=".$_SESSION['admin_id']);
        $this->assign('find_admin_user',$find_admin_user);
        
        $where = '';
        $_GET['username'] = isset($_GET['username'])?$_GET['username']:'';
        if(!empty($_GET['username'])){
            $where .= " and username like '%".$_GET['username']."%'";
        }
        $admin_user_list = AdmingModel::select_admin_user($where);
        $this->assign('admin_user_list',$admin_user_list);
        
        return $this->fetch();
    }
    public function act_del_admin_user()
    {
        $action_arr = AdmingModel::get_action_arr($_SESSION['admin_id']);
        if(!in_array('top',$action_arr)){
            echo '没有该权限'; exit;
        }
        $result = AdmingModel::delete_admin_user("and admin_id='".$_POST['admin_id']."'");
        if($result){
            echo 'success';
        }else{
            echo '操作失败';
        }
    }
    public function add_admin_user()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id=".$_SESSION['admin_id']);
        $this->assign('find_admin_user',$find_admin_user);
        
        return $this->fetch();
    }
    public function act_add_admin_user()
    {
        if($_POST['username']=='' || $_POST['password']==''){
            echo '用户名密码不能为空';
        }
        $data['username'] = $_POST['username'];
        $data['password'] = md5($_POST['password']);
        $result = AdmingModel::add_admin_user($data);
        if($result){
            echo 'success';
        }else{
            echo '操作失败';
        }
    }
    public function edit_admin_user()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id=".$_SESSION['admin_id']);
        $this->assign('find_admin_user',$find_admin_user);
        
        $find_edit_admin_user = AdmingModel::find_admin_user("and admin_id=".$_GET['admin_id']);
        $this->assign('find_edit_admin_user',$find_edit_admin_user);
        
        return $this->fetch();
    }
    public function act_save_admin_user()
    {
        $action_arr = AdmingModel::get_action_arr($_SESSION['admin_id']);
        if(!in_array('top',$action_arr)){
            echo '没有该权限'; exit;
        }
        if($_POST['username']=='' || $_POST['password']==''){
            echo '用户名密码不能为空';
        }
        $where = "and admin_id=".$_POST['admin_id'];
        $data['username'] = $_POST['username'];
        $data['password'] = md5($_POST['password']);
        $result = AdmingModel::save_admin_user($where,$data);
        if($result){
            echo 'success';
        }else{
            echo '操作失败';
        }
    }
    public function message()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id='".$_SESSION['admin_id']."'");
        $this->assign('find_admin_user',$find_admin_user);
        
        $where = ' and type_id=1';
        $_GET['title'] = isset($_GET['title'])?$_GET['title']:'';
        $_GET['contact'] = isset($_GET['contact'])?$_GET['contact']:'';
        $_GET['start_time'] = isset($_GET['start_time'])?$_GET['start_time']:'';
        $_GET['last_time'] = isset($_GET['last_time'])?$_GET['last_time']:'';
        if(!empty($_GET['title'])){
            $where .= " and title like '%".$_GET['title']."%'";
        }
        if(!empty($_GET['contact'])){
            $where .= " and contact like '%".$_GET['contact']."%'";
        }
        $start_time =  strtotime($_GET['start_time']." 00:00:00");
        $last_time = strtotime($_GET['last_time']." 23:59:59");
        if(!empty($_GET['start_time'])){
            $where .= " and add_time>=$start_time";
        }
        if(!empty($_GET['last_time'])){
            $where .= " and add_time<=$last_time";
        }
   
        $limit = !empty($_GET['limit'])?$_GET['limit']:15;
        $page = !empty($_GET['page'])?$_GET['page']:1;
        $params = ['page'=>$page,'query'=>['title'=>$_GET['title'],'contact'=>$_GET['contact'],'start_time'=>$_GET['start_time'],'last_time'=>$_GET['last_time']]];
        $query = db('message')->where("1=1 $where")->order('id desc')->paginate($limit,false,$params);
        $page_show = $query->render();
        $this->assign('page_show',$page_show);
        $this->assign('message_list',$query);
        
        return $this->fetch();
    }
    public function order()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id='".$_SESSION['admin_id']."'");
        $this->assign('find_admin_user',$find_admin_user);
        
        $where = ' and type_id=2';
        $_GET['contact'] = isset($_GET['contact'])?$_GET['contact']:'';
        $_GET['tel'] = isset($_GET['tel'])?$_GET['tel']:'';
        $_GET['start_time'] = isset($_GET['start_time'])?$_GET['start_time']:'';
        $_GET['last_time'] = isset($_GET['last_time'])?$_GET['last_time']:'';
        if(!empty($_GET['title'])){
            $where .= " and title like '%".$_GET['title']."%'";
        }
        if(!empty($_GET['content'])){
            $where .= " and content like '%".$_GET['content']."%'";
        }
        $start_time =  strtotime($_GET['start_time']." 00:00:00");
        $last_time = strtotime($_GET['last_time']." 23:59:59");
        if(!empty($_GET['start_time'])){
            $where .= " and add_time>=$start_time";
        }
        if(!empty($_GET['last_time'])){
            $where .= " and add_time<=$last_time";
        }
   
        $limit = !empty($_GET['limit'])?$_GET['limit']:15;
        $page = !empty($_GET['page'])?$_GET['page']:1;
        $params = ['page'=>$page,'query'=>['contact'=>$_GET['contact'],'tel'=>$_GET['tel'],'start_time'=>$_GET['start_time'],'last_time'=>$_GET['last_time']]];
        $query = db('message')->where("1=1 $where")->order('id desc')->paginate($limit,false,$params);
        $page_show = $query->render();
        $this->assign('page_show',$page_show);
        $this->assign('message_list',$query);
        
        return $this->fetch();
    }
    public function act_del_message()
    {
        $result = AdmingModel::delete_message("and id='".$_POST['id']."'");
        if($result){
            echo 'success';
        }else{
            echo '操作失败';
        }
    }
    public function edit_password()
    {
        $find_admin = AdmingModel::find_admin_user("and username='".$_SESSION['username']."'");
        $this->assign('find_admin',$find_admin);
        return $this->fetch();
    }
    public function act_save_password()
    {
        $where = "and username='".$_SESSION['username']."'";
        $find_admin = AdmingModel::find_admin_user($where);
        $password = md5($_POST['password']);
        if($find_admin['password']!=$password){
            echo '旧密码输入不正确'; exit;
        }
        
        if(empty($_POST['comfirm_password'])){
            echo '新密码不能为空'; exit;
        }
        if(empty($_POST['comfirm_password2'])){
            echo '确认新密码不能为空'; exit;
        }
        if($_POST['comfirm_password']!=$_POST['comfirm_password2']){
            echo '两次密码输入不一至'; exit;
        }
        $data['password'] = md5(trim($_POST['comfirm_password']));
        $data['uptime'] = time();
        $result = AdmingModel::save_admin_user($where,$data);
        if($result){
            echo 'success';
        }else{
            echo '操作失败';
        }
    }
    
    /*
    about 关于我们 catid=1
    goods_list 产品列表 catid=2
    news_cat 新闻分类 catid=3
    service 服务中心 catid=4
    contact 联系我们 catid=5
    banner banner图片上传 catid=9
    link 友情链接 catid=10
    */
    //news_cat 新闻分类 catid=3
    public function news_cat()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id='".$_SESSION['admin_id']."'");
        $this->assign('find_admin_user',$find_admin_user);
 
        $category_tree = AdmingModel::get_category_tree("and catid=3   or parent=3");
        $this->assign('category_tree',$category_tree);
        
        $this->assign('cat_name','新闻分类');
        return $this->fetch('index');
    }
    //add_news 发布新闻 catid=3
    public function add_news()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id=".$_SESSION['admin_id']);
        $this->assign('find_admin_user',$find_admin_user);
        
        $brand_list = AdmingModel::select_brand();
        $this->assign('brand_list',$brand_list);
        
        $category_tree = AdmingModel::get_category_tree("and catid=3  or parent=3");
        $this->assign('category_tree',$category_tree);
        
        $find_home_config = AdmingModel::find_home_config("and id=1");
        $this->assign('find_home_config',$find_home_config);
        
        $this->assign('arclist_status_arr',AdmingModel::arclist_status_arr());
        $this->assign('arclist_flagshow_arr',AdmingModel::arclist_flagshow_arr());
        
        return $this->fetch();
    }
    //news_list 新闻列表 catid=3
    public function news_list()
    {	
        $find_admin_user = AdmingModel::find_admin_user("and admin_id='".$_SESSION['admin_id']."'");
        $this->assign('find_admin_user',$find_admin_user);
        
        $where = '';
        $_GET['title'] = isset($_GET['title'])?$_GET['title']:'';
        $_GET['catid'] = 3;
        $_GET['brand_id'] = isset($_GET['brand_id'])?$_GET['brand_id']:'';
        $_GET['status'] = isset($_GET['status'])?$_GET['status']:'';
        $_GET['flag'] = isset($_GET['flag'])?$_GET['flag']:'';
        $_GET['user_id'] = isset($_GET['user_id'])?$_GET['user_id']:'';
        if(!empty($_GET['title'])){
            $where .= " and title like '%".$_GET['title']."%'";
        }
        if(!empty($_GET['catid'])){
            $child_catid = AdmingModel::getChildCatid($_GET['catid']);
            $where .= " and catid in (".$child_catid.")";
        }
        if(!empty($_GET['brand_id'])){
            $where .= " and brand_id='".$_GET['brand_id']."'";
        }
        if(!empty($_GET['status'])){
            $where .= " and status='".$_GET['status']."'";
        }
        if(!empty($_GET['flag'])){
            $where .= " and flag_".$_GET['flag']."=1";
        }
        if(!empty($_GET['user_id'])){
            $where .= " and user_id='".$_GET['user_id']."'";
        }
        
        $limit = !empty($_GET['limit'])?$_GET['limit']:15;
        $page = !empty($_GET['page'])?$_GET['page']:1;
        $params = ['page'=>$page,'query'=>['title'=>$_GET['title'],'catid'=>$_GET['catid'],'brand_id'=>$_GET['brand_id'],'status'=>$_GET['status'],'flag'=>$_GET['flag'],'user_id'=>$_GET['user_id']]];
        $query = db('arclist')->where("1=1 $where")->order('arcid desc')->paginate($limit,false,$params);
        $page_show = $query->render();
        $this->assign('page_show',$page_show);
        $this->assign('arclist_list',$query);
        
        $brand_list = AdmingModel::select_brand();
        $this->assign('brand_list',$brand_list);
        
        $admin_user_list = AdmingModel::select_admin_user();
        $this->assign('admin_user_list',$admin_user_list);
        
        $category_tree = AdmingModel::get_category_tree();
        $this->assign('category_tree',$category_tree);
        
        $this->assign('arclist_status_arr',AdmingModel::arclist_status_arr());
        $this->assign('arclist_flagshow_arr',AdmingModel::arclist_flagshow_arr());
        
        return $this->fetch();
    }
    //goods_cat 产品分类 catid=2
    public function goods_cat()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id='".$_SESSION['admin_id']."'");
        $this->assign('find_admin_user',$find_admin_user);
 
        $category_tree = AdmingModel::get_category_tree("and catid=2 or parent=2");
        $this->assign('category_tree',$category_tree);
        
        $this->assign('cat_name','产品分类');
        return $this->fetch('index');
    }
    //add_goods 发布产品 catid=2
    public function add_goods()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id=".$_SESSION['admin_id']);
        $this->assign('find_admin_user',$find_admin_user);
        
        $brand_list = AdmingModel::select_brand();
        $this->assign('brand_list',$brand_list);
        
        $category_tree = AdmingModel::get_category_tree("and catid=2 or parent=2");
        $this->assign('category_tree',$category_tree);
        
        $find_home_config = AdmingModel::find_home_config("and id=1");
        $this->assign('find_home_config',$find_home_config);
        
        $this->assign('arclist_status_arr',AdmingModel::arclist_status_arr());
        $this->assign('arclist_flagshow_arr',AdmingModel::arclist_flagshow_arr());
        
        return $this->fetch();
    }
    //goods_list 产品列表 catid=2
    public function goods_list()
    {	
        $find_admin_user = AdmingModel::find_admin_user("and admin_id='".$_SESSION['admin_id']."'");
        $this->assign('find_admin_user',$find_admin_user);
        
        $where = '';
        $_GET['title'] = isset($_GET['title'])?$_GET['title']:'';
        $_GET['catid'] = 2;
        $_GET['brand_id'] = isset($_GET['brand_id'])?$_GET['brand_id']:'';
        $_GET['status'] = isset($_GET['status'])?$_GET['status']:'';
        $_GET['flag'] = isset($_GET['flag'])?$_GET['flag']:'';
        $_GET['user_id'] = isset($_GET['user_id'])?$_GET['user_id']:'';
        if(!empty($_GET['title'])){
            $where .= " and title like '%".$_GET['title']."%'";
        }
        if(!empty($_GET['catid'])){
            $child_catid = AdmingModel::getChildCatid($_GET['catid']);
            $where .= " and catid in (".$child_catid.")";
        }
        if(!empty($_GET['brand_id'])){
            $where .= " and brand_id='".$_GET['brand_id']."'";
        }
        if(!empty($_GET['status'])){
            $where .= " and status='".$_GET['status']."'";
        }
        if(!empty($_GET['flag'])){
            $where .= " and flag_".$_GET['flag']."=1";
        }
        if(!empty($_GET['user_id'])){
            $where .= " and user_id='".$_GET['user_id']."'";
        }
        
        $limit = !empty($_GET['limit'])?$_GET['limit']:15;
        $page = !empty($_GET['page'])?$_GET['page']:1;
        $params = ['page'=>$page,'query'=>['title'=>$_GET['title'],'catid'=>$_GET['catid'],'brand_id'=>$_GET['brand_id'],'status'=>$_GET['status'],'flag'=>$_GET['flag'],'user_id'=>$_GET['user_id']]];
        $query = db('arclist')->where("1=1 $where")->order('arcid desc')->paginate($limit,false,$params);
        $page_show = $query->render();
        $this->assign('page_show',$page_show);
        $this->assign('arclist_list',$query);
        
        $brand_list = AdmingModel::select_brand();
        $this->assign('brand_list',$brand_list);
        
        $admin_user_list = AdmingModel::select_admin_user();
        $this->assign('admin_user_list',$admin_user_list);
        
        $category_tree = AdmingModel::get_category_tree();
        $this->assign('category_tree',$category_tree);
        
        $this->assign('arclist_status_arr',AdmingModel::arclist_status_arr());
        $this->assign('arclist_flagshow_arr',AdmingModel::arclist_flagshow_arr());
        
        return $this->fetch();
    }
    //about 关于我们 catid=1
    public function about()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id='".$_SESSION['admin_id']."'");
        $this->assign('find_admin_user',$find_admin_user);
 
        $category_tree = AdmingModel::get_category_tree("and catid=1");
        $this->assign('category_tree',$category_tree);
        
        $this->assign('cat_name','关于我们');
        return $this->fetch('index');
    }
    //service 服务中心 catid=4
    public function service()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id='".$_SESSION['admin_id']."'");
        $this->assign('find_admin_user',$find_admin_user);
 
        $category_tree = AdmingModel::get_category_tree("and catid=4");
        $this->assign('category_tree',$category_tree);
        
        $this->assign('cat_name','服务中心');
        return $this->fetch('index');
    }
    //contact 联系我们 catid=5
    public function contact()
    {
        $find_admin_user = AdmingModel::find_admin_user("and admin_id='".$_SESSION['admin_id']."'");
        $this->assign('find_admin_user',$find_admin_user);
 
        $category_tree = AdmingModel::get_category_tree("and catid=5");
        $this->assign('category_tree',$category_tree);
        
        $this->assign('cat_name','联系我们');
        return $this->fetch('index');
    }
    //banner banner图片上传 catid=9
    public function banner()
    {	
        $find_admin_user = AdmingModel::find_admin_user("and admin_id='".$_SESSION['admin_id']."'");
        $this->assign('find_admin_user',$find_admin_user);
        
        $where = '';
        $_GET['title'] = isset($_GET['title'])?$_GET['title']:'';
        $_GET['catid'] = 9;
        $_GET['brand_id'] = isset($_GET['brand_id'])?$_GET['brand_id']:'';
        $_GET['status'] = isset($_GET['status'])?$_GET['status']:'';
        $_GET['flag'] = isset($_GET['flag'])?$_GET['flag']:'';
        $_GET['user_id'] = isset($_GET['user_id'])?$_GET['user_id']:'';
        if(!empty($_GET['title'])){
            $where .= " and title like '%".$_GET['title']."%'";
        }
        if(!empty($_GET['catid'])){
            $child_catid = AdmingModel::getChildCatid($_GET['catid']);
            $where .= " and catid in (".$child_catid.")";
        }
        if(!empty($_GET['brand_id'])){
            $where .= " and brand_id='".$_GET['brand_id']."'";
        }
        if(!empty($_GET['status'])){
            $where .= " and status='".$_GET['status']."'";
        }
        if(!empty($_GET['flag'])){
            $where .= " and flag_".$_GET['flag']."=1";
        }
        if(!empty($_GET['user_id'])){
            $where .= " and user_id='".$_GET['user_id']."'";
        }
        
        $limit = !empty($_GET['limit'])?$_GET['limit']:15;
        $page = !empty($_GET['page'])?$_GET['page']:1;
        $params = ['page'=>$page,'query'=>['title'=>$_GET['title'],'catid'=>$_GET['catid'],'brand_id'=>$_GET['brand_id'],'status'=>$_GET['status'],'flag'=>$_GET['flag'],'user_id'=>$_GET['user_id']]];
        $query = db('arclist')->where("1=1 $where")->order('arcid desc')->paginate($limit,false,$params);
        $page_show = $query->render();
        $this->assign('page_show',$page_show);
        $this->assign('arclist_list',$query);
        
        $brand_list = AdmingModel::select_brand();
        $this->assign('brand_list',$brand_list);
        
        $admin_user_list = AdmingModel::select_admin_user();
        $this->assign('admin_user_list',$admin_user_list);
        
        $category_tree = AdmingModel::get_category_tree();
        $this->assign('category_tree',$category_tree);
        
        $this->assign('arclist_status_arr',AdmingModel::arclist_status_arr());
        $this->assign('arclist_flagshow_arr',AdmingModel::arclist_flagshow_arr());
        
        return $this->fetch('arclist');
    }
    //link 友情链接 catid=10
    public function link()
    {	
        $find_admin_user = AdmingModel::find_admin_user("and admin_id='".$_SESSION['admin_id']."'");
        $this->assign('find_admin_user',$find_admin_user);
        
        $where = '';
        $_GET['title'] = isset($_GET['title'])?$_GET['title']:'';
        $_GET['catid'] = 10;
        $_GET['brand_id'] = isset($_GET['brand_id'])?$_GET['brand_id']:'';
        $_GET['status'] = isset($_GET['status'])?$_GET['status']:'';
        $_GET['flag'] = isset($_GET['flag'])?$_GET['flag']:'';
        $_GET['user_id'] = isset($_GET['user_id'])?$_GET['user_id']:'';
        if(!empty($_GET['title'])){
            $where .= " and title like '%".$_GET['title']."%'";
        }
        if(!empty($_GET['catid'])){
            $child_catid = AdmingModel::getChildCatid($_GET['catid']);
            $where .= " and catid in (".$child_catid.")";
        }
        if(!empty($_GET['brand_id'])){
            $where .= " and brand_id='".$_GET['brand_id']."'";
        }
        if(!empty($_GET['status'])){
            $where .= " and status='".$_GET['status']."'";
        }
        if(!empty($_GET['flag'])){
            $where .= " and flag_".$_GET['flag']."=1";
        }
        if(!empty($_GET['user_id'])){
            $where .= " and user_id='".$_GET['user_id']."'";
        }
        
        $limit = !empty($_GET['limit'])?$_GET['limit']:15;
        $page = !empty($_GET['page'])?$_GET['page']:1;
        $params = ['page'=>$page,'query'=>['title'=>$_GET['title'],'catid'=>$_GET['catid'],'brand_id'=>$_GET['brand_id'],'status'=>$_GET['status'],'flag'=>$_GET['flag'],'user_id'=>$_GET['user_id']]];
        $query = db('arclist')->where("1=1 $where")->order('arcid desc')->paginate($limit,false,$params);
        $page_show = $query->render();
        $this->assign('page_show',$page_show);
        $this->assign('arclist_list',$query);
        
        $brand_list = AdmingModel::select_brand();
        $this->assign('brand_list',$brand_list);
        
        $admin_user_list = AdmingModel::select_admin_user();
        $this->assign('admin_user_list',$admin_user_list);
        
        $category_tree = AdmingModel::get_category_tree();
        $this->assign('category_tree',$category_tree);
        
        $this->assign('arclist_status_arr',AdmingModel::arclist_status_arr());
        $this->assign('arclist_flagshow_arr',AdmingModel::arclist_flagshow_arr());
        
        return $this->fetch('arclist');
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //产品统计
    public function flux_goods()
    {
        $where = ' and type_id=1';
        $_GET['start_time'] = isset($_GET['start_time'])?$_GET['start_time']:'';
        $_GET['last_time'] = isset($_GET['last_time'])?$_GET['last_time']:'';
        $start_time =  strtotime($_GET['start_time']." 00:00:00");
        $last_time = strtotime($_GET['last_time']." 23:59:59");
        if(!empty($_GET['start_time'])){
            $where .= " and intime>=$start_time";
        }
        if(!empty($_GET['last_time'])){
            $where .= " and intime<=$last_time";
        }
   
        $limit = !empty($_GET['limit'])?$_GET['limit']:15;
        $page = !empty($_GET['page'])?$_GET['page']:1;
        $params = ['page'=>$page,'query'=>['start_time'=>$_GET['start_time'],'last_time'=>$_GET['last_time']]];
        $query = db('arcflux')->where("1=1 $where")->order('id desc')->paginate($limit,false,$params);
        $page_show = $query->render();
        $this->assign('page_show',$page_show);
        $this->assign('lists',$query);
        
        return $this->fetch();
    }
    //新闻统计
    public function flux_news()
    {
        $where = ' and type_id=2';
        $_GET['start_time'] = isset($_GET['start_time'])?$_GET['start_time']:'';
        $_GET['last_time'] = isset($_GET['last_time'])?$_GET['last_time']:'';
        $start_time =  strtotime($_GET['start_time']." 00:00:00");
        $last_time = strtotime($_GET['last_time']." 23:59:59");
        if(!empty($_GET['start_time'])){
            $where .= " and intime>=$start_time";
        }
        if(!empty($_GET['last_time'])){
            $where .= " and intime<=$last_time";
        }
   
        $limit = !empty($_GET['limit'])?$_GET['limit']:15;
        $page = !empty($_GET['page'])?$_GET['page']:1;
        $params = ['page'=>$page,'query'=>['start_time'=>$_GET['start_time'],'last_time'=>$_GET['last_time']]];
        $query = db('arcflux')->where("1=1 $where")->order('id desc')->paginate($limit,false,$params);
        $page_show = $query->render();
        $this->assign('page_show',$page_show);
        $this->assign('lists',$query);
        
        return $this->fetch();
    }
    
    //网站访问统计
    public function flux_web()
    {
        return $this->fetch();
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function login()
    {
        return $this->fetch();
    }
    public function actlogin()
    {
        $username = trim($_POST['username']);
        $password = md5(trim($_POST['password']));
        
        $find_admin = AdmingModel::find_admin_user("and username='".$username."' and password='".$password."'");
        if($find_admin){
            $_SESSION['username'] = $find_admin['username'];
            $_SESSION['admin_id'] = $find_admin['admin_id'];
            echo 'success';
        }else{
            echo 'error';
        }
    }
    public function logout()
    {
        unset($_SESSION['username']);
        $this->redirect('/home.php/adming/index/login.html');
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
