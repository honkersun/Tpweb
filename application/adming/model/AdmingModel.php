<?php
namespace app\adming\model;

use think\Model;
use think\Db;

class AdmingModel extends Model{
    
    public static function find_admin_user($where=''){
        $admin_user = db('admin_user');
        $find = $admin_user->where("1=1 $where")->find();
        return $find;
    }
    public static function select_admin_user($where=''){
        $admin_user = db('admin_user');
        $query = $admin_user->where("1=1 $where")->select();
        return $query;
    }
    public static function save_admin_user($where,$data){
        $admin_user = db('admin_user');
        $result = $admin_user->where("1=1 $where")->update($data);
        return $result;
    }
    public static function find_category($where=''){
        $category = db('category');
        $find = $category->where("1=1 $where")->find();
        return $find;
    }
    public static function add_category($data){
        $category = db('category');
        $result = $category->insertGetId($data);
        return $result;
    }
    public static function select_category($where=''){
        $category = db('category');
        $query = $category->where("1=1 $where")->order('sort asc')->select();
        return $query;
    }
    public static function get_category_tree($where=''){
        $category_list = self::select_category($where);
        $tree_data = self::getTree($category_list);
        return $tree_data;
    }
    /*
        * 生成无限级目录
        * @param array $category 数据源结构： array(array('id'=>1,'parent_id'=>0,'name'=>'一'),array('id'=>2,'parent_id'=>2,'name'=>'2一'))
        * @param string $id id字段名
        * @param string $pid 父id字段名
        * @param string $son
        * @return array
    */
    public static function getTree($category = array(),$id='catid',$pid='parent',$son = 'child'){
        $treeArr = array(); //格式化的树
        $tmpMap = array();  //临时扁平数据
        foreach ($category as $val){
            $tmpMap[$val[$id]] = $val;
        }
        foreach($category as $val){
            $tmp_id = $val[$id];
            if (isset($tmpMap[$val[$pid]])) {
                $tmpMap[$val[$pid]][$son][] = &$tmpMap[$tmp_id];
            } else {
                $treeArr[] = &$tmpMap[$tmp_id];
            }
        }
        return $treeArr;
    }
    public static function save_category($where,$data){
        $category = db('category');
        $result = $category->where("1=1 $where")->update($data);
        return $result;
    }
    public static function save_arclist($where,$data){
        $arclist = db('arclist');
        $result = $arclist->where("1=1 $where")->update($data);
        return $result;
    }
    public static function delete_category($where=''){
        $category = db('category');
        $result = $category->where("1=1 $where")->delete();
        return $result;
    }
    public static function delete_arclist($where=''){
        $arclist = db('arclist');
        $result = $arclist->where("1=1 $where")->delete();
        return $result;
    }
    public static function select_brand($where=''){
        $brand = db('brand');
        $query = $brand->where("1=1 $where")->order('sort asc')->select();
        return $query;
    }
    public static function add_brand($data){
        $brand = db('brand');
        $result = $brand->insertGetId($data);
        return $result;
    }
    public static function find_brand($where=''){
        $brand = db('brand');
        $find = $brand->where("1=1 $where")->find();
        return $find;
    }
    public static function find_arclist($where=''){
        $arclist = db('arclist');
        $find = $arclist->where("1=1 $where")->find();
        return $find;
    }
    public static function save_brand($where,$data){
        $brand = db('brand');
        $result = $brand->where("1=1 $where")->update($data);
        return $result;
    }
    public static function delete_brand($where=''){
        $brand = db('brand');
        $result = $brand->where("1=1 $where")->delete();
        return $result;
    }
    /**
     * $upfile=$_FILES['image'],$_FILES['attachment'];
     * 前台html的form表签必须enctype="multipart/form-data"
     */
    public static function upload_file($upfile,$id){
        if(empty($upfile)){
            return '';
        }
        $name = $upfile['name'];             //文件名
        $type = $upfile['type'];             //文件类型
        $size = $upfile['size'];             //文件大小
        $tmp_name = $upfile['tmp_name'];     //临时文件
        $error = $upfile['error'];         //出错原因
        //尺寸判断
        if($size==0){
            return '';
        }
        //文件路径
        $destination_folder = 'uploads/';
        if (!file_exists($destination_folder)){
            mkdir($destination_folder);
        }
        $pinfo = pathinfo($name);
        $ftype = $pinfo['extension'];
		//文件命名
        $destination = $destination_folder.time().rand(1000,9999).$id.".".$ftype;
        if (file_exists($destination) && $overwrite != true){
            echo '同名的文件已经存在了'; exit;
        }
        if (!move_uploaded_file($tmp_name, $destination)){
            echo '移动文件出错'; exit;
        }
        $pinfo = pathinfo($destination);
        $fname = $pinfo['basename'];
        //文件全路径
        $image_file = '/'.$destination_folder.$fname;
        //返回
        return $image_file;
    }
    public static function add_arclist($data){
        $arclist = db('arclist');
        $result = $arclist->insertGetId($data);
        return $result;
    }
    public static function select_arclist($where,$page,$limit=15){
        $arclist = db('arclist');
        $query = $arclist->where("1=1 $where")->order('arcid desc')->page($page,$limit)->select();
        return $query;
    }
    public static function count_arclist($where=''){
        $arclist = db('arclist');
        $count = $arclist->where("1=1 $where")->count();
        return $count;
    }
    public static function get_flag_str($a,$b,$c,$d,$e){
        $str = '';
        if($a==1){
            $str .= 'a,';
        }
        if($b==1){
            $str .= 'b,';
        }
        if($c==1){
            $str .= 'c,';
        }
        if($d==1){
            $str .= 'd,';
        }
        if($e==1){
            $str .= 'e,';
        }
        return substr($str, 0, -1);
    }
    public static function find_home_config($where=''){
        $home_config = db('home_config');
        $find = $home_config->where("1=1 $where")->find();
        return $find;
    }
    public static function save_home_config($where,$data){
        $home_config = db('home_config');
        $result = $home_config->where("1=1 $where")->update($data);
        return $result;
    }
    public static function delete_admin_user($where=''){
        $admin_user = db('admin_user');
        $result = $admin_user->where("1=1 $where")->delete();
        return $result;
    }
    public static function add_admin_user($data){
        $admin_user = db('admin_user');
        $result = $admin_user->insertGetId($data);
        return $result;
    }
    public static function get_action_arr($admin_id){
        $find_admin_user = self::find_admin_user("and admin_id=$admin_id");
        return explode(',',$find_admin_user['action_list']);
    }
    public static function select_message($where,$page,$limit=15){
        $message = db('message');
        $query = $message->where("1=1 $where")->order('id desc')->page($page,$limit)->select();
        return $query;
    }
    public static function count_message($where=''){
        $message = db('message');
        $count = $message->where("1=1 $where")->count();
        return $count;
    }
    public static function delete_message($where=''){
        $message = db('message');
        $result = $message->where("1=1 $where")->delete();
        return $result;
    }
	public static function add_message($data){
        $message = db('message');
        $result = $message->insertGetId($data);
        return $result;
    }
    //取子级类目catid
    public static function getChildCatid($catid){
        //一级二级
        $category_list = self::select_category("and parent in($catid)");
        $catid_str = $catid.',';
        foreach($category_list as $val){
            $catid_str .= $val['catid'].',';
        }
        $catid_str = substr($catid_str, 0, -1);
        unset($category_list);
        
        //三级
        $category_list = self::select_category("and parent in($catid_str)");
        $catid_str = $catid_str.',';
        foreach($category_list as $val){
            $catid_str .= $val['catid'].',';
        }
        $catid_str = substr($catid_str, 0, -1);
        unset($category_list);

        //此时已经有很多重复的catid需要过滤一下
        $catid_arr = array_unique(array_filter(explode(',',$catid_str)));
        $catid_str=implode(',',$catid_arr);
        
        //四级
        $category_list = self::select_category("and parent in($catid_str)");
        $catid_str = $catid_str.',';
        foreach($category_list as $val){
            $catid_str .= $val['catid'].',';
        }
        $catid_str = substr($catid_str, 0, -1);
        unset($category_list);
        
        //五级
        $category_list = self::select_category("and parent in($catid_str)");
        $catid_str = $catid_str.',';
        foreach($category_list as $val){
            $catid_str .= $val['catid'].',';
        }
        $catid_str = substr($catid_str, 0, -1);
        unset($category_list);
        
        //此时已经有很多重复的catid需要过滤一下
        $catid_arr = array_unique(array_filter(explode(',',$catid_str)));
        $catid_str=implode(',',$catid_arr);
        
        return $catid_str;
    }
    
    /*
     * arcflux
     */
    public static function find_arcflux($where=''){
        return db('arcflux')->where("1=1 $where")->find();
    }
    public static function select_arcflux($where=''){
        return db('arcflux')->where("1=1 $where")->order('id asc')->select();
    }
    public static function del_arcflux($where){
        return db('arcflux')->where("1=1 $where")->delete();
    }
    public static function edit_arcflux($where,$data){
        return db('arcflux')->where("1=1 $where")->update($data);
    }
    public static function add_arcflux($data){
        return db('arcflux')->insertGetId($data);
    }
    public static function count_arcflux($where=''){
        return db('arcflux')->where("1=1 $where")->count();
    }
    /*
     * iplog
     */
    public static function find_iplog($where=''){
        return db('iplog')->where("1=1 $where")->find();
    }
    public static function select_iplog($where=''){
        return db('iplog')->where("1=1 $where")->order('id asc')->select();
    }
    public static function del_iplog($where){
        return db('iplog')->where("1=1 $where")->delete();
    }
    public static function edit_iplog($where,$data){
        return db('iplog')->where("1=1 $where")->update($data);
    }
    public static function add_iplog($data){
        return db('iplog')->insertGetId($data);
    }
    public static function count_iplog($where=''){
        return db('iplog')->where("1=1 $where")->count();
    }
    
    
    /*
     * status
     */
    public static function arclist_status_arr()
    {
        $arr[1] = '已审核';
        $arr[2] = '未审核';
        return $arr;
    }
    public static function arclist_status_text($key)
    {
        $arr = self::arclist_status_arr();
        if(!isset($arr[$key])){
            return '';
        }
        return $arr[$key];
    }
    /*
     * flag
     */
    public static function arclist_flag_arr()
    {
        $arr['1'] = 'a';
        $arr['2'] = 'b';
        $arr['3'] = 'c';
        $arr['4'] = 'd';
        $arr['5'] = 'e';
        return $arr;
    }
    public static function arclist_flag_text($key)
    {
        $arr = self::arclist_flag_arr();
        if(!isset($arr[$key])){
            return '';
        }
        return $arr[$key];
    }
    public static function arclist_flagshow_arr()
    {
        $arr['a'] = '推荐';
        $arr['b'] = '特荐';
        $arr['c'] = '热门';
        $arr['d'] = '头条';
        $arr['e'] = '普通';
        return $arr;
    }
    public static function arclist_flagshow_text($key)
    {
        $arr = self::arclist_flagshow_arr();
        if(!isset($arr[$key])){
            return '';
        }
        return $arr[$key];
    }
    /*
     * status
     */
    public static function category_nav_arr()
    {
        $arr[1] = '显示';
        $arr[2] = '不显示';
        return $arr;
    }
    public static function category_nav_text($key)
    {
        $arr = self::category_nav_arr();
        if(!isset($arr[$key])){
            return '';
        }
        return $arr[$key];
    }
    
    
    
    /*
     * query只做查询
     * execute可做插入更新删除
     */
    public static function querysql($sql){
        return Db::query($sql);
    }
    public static function printr($arr){
        header("Content-Type:text/html;charset=UTF-8");
        echo '<pre>';
        print_r($arr);
        echo '</pre>';
        exit;
    }
}
?>