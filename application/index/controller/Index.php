<?php
namespace app\index\controller;

use think\Controller;
use app\index\model\IndexModel;
use app\adming\model\AdmingModel;

class Index extends Controller
{
    public function index(){
        
        $find_home_config = IndexModel::find_home_config("and id=1");
        $this->assign('find_home_config',$find_home_config);
        
        return $this->fetch();
    }
    public function cat(){
        $where = '';
        $catid = '';
        $q = '';
        $fetch = 'list';
        //列表页
        if(!empty($_GET['catid'])){
            $catid = trim($_GET['catid']);
            $child_catid = AdmingModel::getChildCatid($_GET['catid']);
            $where .= "and catid in (".$child_catid.")";
        }
        //搜索页
        if(!empty($_GET['q'])){
            $q = trim($_GET['q']);
            $where .= " and (title like '%".$q."%' or context like '%".$q."%')";
            $fetch = 'search_list';
        }
        
        $limit = !empty($_GET['limit'])?$_GET['limit']:9;
        $page = !empty($_GET['page'])?$_GET['page']:1;
        $params = ['page'=>$page,'query'=>['catid'=>$catid,'q'=>$q]];
        $query = db('arclist')->where("1=1 $where")->order('sort desc')->paginate($limit,false,$params);
        $page_show = $query->render();
        $this->assign('page_show',$page_show);
        $this->assign('arclist_list',$query);
        
        $find_category = IndexModel::find_category("and catid='".$catid."'");
        $this->assign('find_category',$find_category);
        
        if($find_category['url_name']){
            $fetch = $find_category['url_name'];
        }
        
        return $this->fetch($fetch);
    }
    public function detail(){
        $find_arclist = IndexModel::find_arclist("and arcid='".$_GET['arcid']."'");
        $this->assign('find_arclist',$find_arclist);
        
        //ip pv uv记录
        $day = date("Y-m-d",time());
        $ip = getenv('REMOTE_ADDR');
        $arcid = $find_arclist['arcid'];
        $params = [];
        $findip = AdmingModel::find_iplog("and ip='".$ip."' and day='".$day."'");
        if(empty($findip)){
            $find_arcflux = AdmingModel::find_arcflux("and type_id=1 and day='".$day."'");
            if($find_arcflux){
                $params['ip'] = $find_arcflux['ip']+1;
                $params['uv'] = $find_arcflux['uv']+1;
            }
        }
        
        $find_iplog = AdmingModel::find_iplog("and arcid=$arcid and ip='".$ip."' and day='".$day."'");
        if(empty($find_iplog)){
            $data = [];
            $data['ip'] = $ip;
            $data['arcid'] = $arcid;
            $data['day'] = $day;
            AdmingModel::add_iplog($data);
        }
        
        $find_arcflux = AdmingModel::find_arcflux("and type_id=1 and day='".$day."'");
        if(empty($find_arcflux)){
            $data = [];
            $data['type_id'] = 1;
            $data['pv'] = 1;
            $data['ip'] = 1;
            $data['uv'] = 1;
            $data['day'] = $day;
            $data['intime'] = time();
            AdmingModel::add_arcflux($data);
        }else{
            $params['pv'] = $find_arcflux['pv']+1;
            AdmingModel::edit_arcflux("and id=".$find_arcflux['id'],$params);
        }
        
        return $this->fetch();
    }
    public function news_detail(){
        $find_arclist = IndexModel::find_arclist("and arcid='".$_GET['arcid']."'");
        $this->assign('find_arclist',$find_arclist);
        
        //ip pv uv记录
        $day = date("Y-m-d",time());
        $ip = getenv('REMOTE_ADDR');
        $arcid = $find_arclist['arcid'];
        $params = [];
        $findip = AdmingModel::find_iplog("and ip='".$ip."' and day='".$day."'");
        if(empty($findip)){
            $find_arcflux = AdmingModel::find_arcflux("and type_id=2 and day='".$day."'");
            if($find_arcflux){
                $params['ip'] = $find_arcflux['ip']+1;
                $params['uv'] = $find_arcflux['uv']+1;
            }
        }
        
        $find_iplog = AdmingModel::find_iplog("and arcid=$arcid and ip='".$ip."' and day='".$day."'");
        if(empty($find_iplog)){
            $data = [];
            $data['ip'] = $ip;
            $data['arcid'] = $arcid;
            $data['day'] = $day;
            AdmingModel::add_iplog($data);
        }
        
        $find_arcflux = AdmingModel::find_arcflux("and type_id=2 and day='".$day."'");
        if(empty($find_arcflux)){
            $data = [];
            $data['type_id'] = 2;
            $data['pv'] = 1;
            $data['ip'] = 1;
            $data['uv'] = 1;
            $data['day'] = $day;
            $data['intime'] = time();
            AdmingModel::add_arcflux($data);
        }else{
            $params['pv'] = $find_arcflux['pv']+1;
            AdmingModel::edit_arcflux("and id=".$find_arcflux['id'],$params);
        }
        
        return $this->fetch();
    }
    public function act_add_message(){
        header("Content-Type:text/html;charset=UTF-8");
        $data['type_id'] = $_POST['type_id'];
        $data['title'] = $_POST['title'];//姓名
        $data['contact'] = $_POST['contact'];//邮箱
        $data['content'] = $_POST['content'];//内容
        $data['add_time'] = time();
        $result = AdmingModel::add_message($data);
        if($result){
            echo 'success';
        }else{
            echo 'fail';
        }
    }   
}
