<?php
namespace app\index\model;

use think\Model;
use think\Db;
use app\adming\model\AdmingModel;

class IndexModel extends Model{
    
    public static function find_category($where){
        $category = db('category');
        $find = $category->where("1=1 $where")->find();
        return $find;
    }
    public static function select_category($where){
        $category = db('category');
        $query = $category->where("1=1 $where")->order('sort asc')->select();
        return $query;
    }
    public static function get_category_tree($where=''){
        $category_list = self::select_category($where);
        $tree_data = self::getTree($category_list);
        return $tree_data;
    }
    /*
        * 生成无限级目录
        * @param array $category 数据源结构： array(array('id'=>1,'parent_id'=>0,'name'=>'一'),array('id'=>2,'parent_id'=>2,'name'=>'2一'))
        * @param string $id id字段名
        * @param string $pid 父id字段名
        * @param string $son
        * @return array
    */
    public static function getTree($category = array(),$id='catid',$pid='parent',$son = 'child'){
        $treeArr = array(); //格式化的树
        $tmpMap = array();  //临时扁平数据
        foreach ($category as $val){
            $tmpMap[$val[$id]] = $val;
        }
        foreach($category as $val){
            $tmp_id = $val[$id];
            if (isset($tmpMap[$val[$pid]])) {
                $tmpMap[$val[$pid]][$son][] = &$tmpMap[$tmp_id];
            } else {
                $treeArr[] = &$tmpMap[$tmp_id];
            }
        }
        return $treeArr;
    }
    public static function select_brand($where){
        $brand = db('brand');
        $query = $brand->where("1=1 $where")->order('sort asc')->select();
        return $query;
    }
    public static function find_brand($where){
        $brand = db('brand');
        $find = $brand->where("1=1 $where")->find();
        return $find;
    }
    public static function find_arclist($where){
        $arclist = db('arclist');
        $find = $arclist->where("1=1 $where")->find();
        return $find;
    }
    public static function select_arclist($parent_catid='',$arc_where='',$sort='update_time desc',$limit){
        $cat_where = '';
        if($parent_catid){
            $child_catid = AdmingModel::getChildCatid($parent_catid);
            $cat_where = "and catid in (".$child_catid.")";
        }
        $arclist = db('arclist');
        $query = $arclist->where("status=1 $cat_where $arc_where")->order($sort)->limit($limit)->select();
        return $query;
    }
    public static function count_arclist($where){
        $arclist = db('arclist');
        $count = $arclist->where("1=1 $where")->count();
        return $count;
    }
    public static function find_home_config($where){
        $home_config = db('home_config');
        $find = $home_config->where("1=1 $where")->find();
        return $find;
    }
    public static function select_message($where,$page,$limit=15){
        $message = db('message');
        $query = $message->where("1=1 $where")->order('id desc')->page($page,$limit)->select();
        return $query;
    }
    public static function count_message($where){
        $message = db('message');
        $count = $message->where("1=1 $where")->count();
        return $count;
    }
    
    
    /*
     * query只做查询
     * execute可做插入更新删除
     */
    public function querysql($sql){
        return Db::query($sql);
    }
    public function printr($arr){
        header("Content-Type:text/html;charset=UTF-8");
        echo '<pre>';
        print_r($arr);
        echo '</pre>';
        exit;
    }
}
?>