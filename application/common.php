<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
use think\Db;

function inputStr($string){
  $string = preg_replace('/[\\x00-\\x08\\x0B\\x0C\\x0E-\\x1F]/','',$string); 
  $string = str_replace(array("\0","%00","\r"),'',$string); 
  $string = preg_replace("/&(?!(#[0-9]+|[a-z]+);)/si",'&',$string); 
  $string = str_replace(array("%3C",'<'),'<',$string); 
  $string = str_replace(array("%3E",'>'),'>',$string); 
  $string = str_replace(array('"',"'","\t",' '),array('"',"'",' ',' '),$string); 
  $string=trim($string); 
  return htmlspecialchars($string, ENT_QUOTES);
}
function bytesToSize1024($bytes, $precision = 2) {
    $unit = array('B','KB','MB');
    return @round($bytes / pow(1024, ($i = floor(log($bytes, 1024)))), $precision).' '.$unit[$i];
}
function send_sms($mobile,$smscode)
{
    header('Content-Type: text/plain; charset=utf-8');
    
    vendor('aliyunsms.api_demo.SmsDemo');
    $sms = new \SmsDemo();
    
    $response = $sms->sendSms($mobile,$smscode);
    if($response->Code=='OK'){
        return true;
    }else{
        return false;
    }
}
function send_mail($smtp_host,$port,$username,$pass,$address,$title,$content,$fromname)
{
    vendor('phpmailer.PHPMailerAutoload');
    //vendor('PHPMailer.class#PHPMailer');
    $mail = new \PHPMailer();          
     // 设置PHPMailer使用SMTP服务器发送Email
    $mail->IsSMTP();   
    // 是否启用smtp的debug进行调试 开发环境建议开启 生产环境注释掉即可 默认关闭debug调试模式
    $mail->SMTPDebug = 1;
    // 设置邮件的字符编码，若不指定，则为'UTF-8'
    $mail->CharSet='UTF-8';         
    // 添加收件人地址，可以多次使用来添加多个收件人
    $mail->AddAddress($address); 
    // 设置邮件正文
    $mail->Body=$content;           
    // 设置邮件头的From字段。
    $mail->From=$username;  
    // 设置发件人名字
    $mail->FromName=$fromname;  
    // 设置邮件标题
    $mail->Subject=$title;          
    // 设置SMTP服务器。
    $mail->Host=$smtp_host;
    $mail->Port=$port;
    // 设置为"需要验证" ThinkPHP 的config方法读取配置文件
    $mail->SMTPAuth=true;
    //设置html发送格式
    $mail->isHTML(true);           
    // 设置用户名和密码。
    $mail->Username=$username;
    $mail->Password=$pass; 
    // 发送邮件。
    return($mail->Send());
}
function printr($arr){
    header("Content-Type:text/html;charset=UTF-8");
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
    exit;
}

/*
    $accountNo = '6225887854837190';
    $idCard = '43042419961006101X';
    $name = '张三';
    $return = bank3Check($accountNo,$idCard,$name);
    if($return['status']=='01'){
        echo $return['msg'];
    }else{
        echo $return['msg'];
    }
 */
function bank3Check($accountNo,$idCard,$name){
    $host = "https://bcard3and4.market.alicloudapi.com";
    $path = "/bank3Check";
    $method = "GET";
    $appcode = "0696914ad4c34770911a3b506c6aa3c2";
    $headers = array();
    array_push($headers, "Authorization:APPCODE " . $appcode);
    $querys = "accountNo=$accountNo&idCard=$idCard&name=$name";
    $bodys = "";
    $url = $host . $path . "?" . $querys;

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_FAILONERROR, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    //curl_setopt($curl, CURLOPT_HEADER, true); 如不输出json, 请打开这行代码，打印调试头部状态码。
    //状态码: 200 正常；400 URL无效；401 appCode错误； 403 次数用完； 500 API网管错误
    if (1 == strpos("$".$host, "https://"))
    {
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    }
    $out_put = curl_exec($curl);
    
    return json_decode($out_put,true);
}

function httpPost($url,$data){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    $res = curl_exec($curl);
    curl_close($curl);
    return json_decode($res,true);
}

function httpGet($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_URL, $url);
    $res = curl_exec($curl);
    curl_close($curl);
    return json_decode($res,true);
}

function getAccessToken($url) {
    $res = httpGet($url);
    return $res['access_token'];
}

function montharr($a,$b){
    if($a>$b){
        return [];
    }
    $rt=[];
    for($a;$a<$b;){
        $rt[]=$a;
        if($a<=2){
            $a=3;
        }elseif($a<12){
            $a+=3;
        }else if($a<24){
            $a+=6;
        }else{
            $a+=12;
        }
    }
    $rt[]=$b;
    return $rt;
}

function nonceStr($length){
    $str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJK1NGJBQRSTUVWXYZ';//随即串，62个字符
    $strlen = 62;
    while($length > $strlen){
    $str .= $str;
    $strlen += 62;
    }
    $str = str_shuffle($str);
    return substr($str,0,$length);
}
function getWxConfig($jsapiTicket,$url,$timestamp,$nonceStr,$appid) {
    $string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";
    $signature = sha1($string);

    $WxConfig["appId"] = $appid;
    $WxConfig["nonceStr"] = $nonceStr;
    $WxConfig["timestamp"] = $timestamp;
    $WxConfig["url"] = $url;
    $WxConfig["signature"] = $signature;
    $WxConfig["rawString"] = $string;
    return $WxConfig;
}

function httpPost2($url,$data)
{
    ////发起请求
    //构建参数
    $header[] = "Content-type:application/x-www-form-urlencoded";        //定义content-type为xml,注意是数组和form表单提交一样
    //初始一个curl会话
    $ch = curl_init($url);
    //设置
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    //curl_setopt($ch, CURLOPT_ENCODING, "utf8");
    curl_setopt($ch,CURLOPT_HTTPHEADER,$header);
    curl_setopt($ch,CURLOPT_POST,1);//设置发送方式：post        
    curl_setopt($ch,CURLOPT_POSTFIELDS,$data);//设置发送数据
    curl_setopt($ch,CURLOPT_TIMEOUT,60);//设置超时时间

    $res = curl_exec($ch);//抓取URL并把它传递给浏览器

    return json_decode($res,true);
}